exports.success = function (req, res, parada, status) {
  console.log(parada)
  // operadores || si no viene esto hace esto
  res.status(status || 200).send({
    error: "",
    body: parada,
  });
};
exports.error = function (req, res, message, status, details) {
  console.error("[response error]" + details);

  res.status(status || 500).send({
    error: message,
    body: "",
  });
};
