const express = require("express");
const info = require("../components/curso/network");
const user = require("../components/user/network");
const algebra = require("../components/curso/algebra/network");
const matematica = require("../components/curso/matematica/network");
const sor = require("../components/curso/sor/network");

const routes = function (server) {
  server.use("/user", user);
  server.use("/curso", info);
  server.use("/algebra", algebra);
  server.use("/matematica", matematica);
  server.use("/sor", sor);
};

module.exports = routes;
