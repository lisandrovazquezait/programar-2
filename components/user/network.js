const express = require("express");
const response = require("../../network/response");
const controller = require("./controller");
const router = express.Router();

router.post("/", function (req, res) {
  controller
    .addUser(req.body.user, req.body.info, req.body.tipo)
    .then((fullMessage) => {
      response.success(req, res, fullMessage, 201);
    })
    .catch((e) => {
      response.error(
        req,
        res,
        "Informacion invalida",
        400,
        "Error en el controfller"
      );
    });
});

router.patch("/:_id", function (req, res) {
  let id = req.params;
  controller
    .updateUser(id, req.body.info, req.body.tipo)
    .then((data) => {
      response.success(req, res, data, 200);
    })
    .catch((e) => {
      response.error(req, res, "Error Interno network", 501, e);
    });
});

router.delete("/:_id", function (req, res) {
  const id = req.params;
  controller
    .deleteUser(id)

    .then((tipo) => {
      response.success(req, res, `Usuario eliminado`, 200);
    })
    .catch((e) => {
      response.error(req, res, "Error interno", 500, e);
    });
});

module.exports = router;
