const Model = require("./model");

async function addUser(usuario) {
  const myUser = new Model(usuario);
  myUser.save();
}
async function updateUser(user, info, tipo) {
  console.log(user);

  const foundMessage = await Model.findByIdAndUpdate(user);
  foundMessage.tipo += tipo;
  foundMessage.info += ", " + info;

  const newMessage = await foundMessage.save();

  return newMessage;
}

async function deleteUser(id) {
  const pepe = Model.findById(id);
  console.log(pepe.user);
  Model.findByIdAndDelete(id);
  name = pepe.user;
  return Model.findByIdAndDelete(id);
}

module.exports = {
  add: addUser,
  update: updateUser,
  delete: deleteUser,
};
