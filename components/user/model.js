const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const myUser = new Schema({
  user: String,
  info: {
    type: String,
    required: true,
  },
  tipo: String,
});

const model = mongoose.model("usuarios", myUser);
module.exports = model;
