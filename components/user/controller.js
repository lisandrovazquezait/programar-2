const store = require("./store");
const { use } = require("../curso/algebra/network");

function addUser(name, info, tipo) {
  return new Promise((resolve, reject) => {
    const usuario = {
      user: name,
      info: info,
      tipo: tipo,
    };
    store.add(usuario);

    resolve(usuario);
  });
}

function updateUser(id, info, tipo) {
  return new Promise(async (resolve, reject) => {
    console.log(id, info, tipo);
    const result = await store.update(id, info, tipo);
    console.log("llego");
    resolve(result);
  });
}

function deleteUser(id) {
  return new Promise((resolve, reject) => {
    store.delete(id);

    resolve(name);
  });
}

module.exports = {
  addUser,
  updateUser,
  deleteUser,
};
