const store = require("./store");

function addSor(tipo, alumno, info, actividad) {
  return new Promise((resolve, reject) => {
    const data = {
      tipo: tipo,
      alumno: alumno,
      info: info,
      actividad: actividad,
    };
    store.add(data);

    resolve(data);
  });
}

function getSor(tipo, actividad, alumno, info, rango) {
  console.log(tipo, actividad, alumno, info);
  return new Promise((resolve, reject) => {
    if (null !== tipo || actividad || alumno || info) {
      resolve(store.list(tipo, actividad, alumno, info));
    } else if (rango === "profesor") {
      let prof = "prof";
      resolve(store.list(prof));
    } else {
      reject(`no hay filtro`);
    }
  });
}

function updateSor(id, actividad, info, rango) {
  return new Promise(async (resolve, reject) => {
    if (!id) {
      reject("[controller.js updateMessage]Invalida data");
      resolve(id);
      return false;
    }
    const result = await store.update(id, actividad, info, rango);
    resolve(result);
  });
}
function deleteSor(id) {
  return new Promise((resolve, reject) => {
    if (!id) {
      reject("id invalido");
      return false;
    }
    store
      .delete(id)
      .then((tipo) => {
        resolve(tipo);
      })
      .catch((e) => {
        reject(e);
      });
  });
}
module.exports = {
  addSor,
  getSor,
  updateSor,
  deleteSor,
};
