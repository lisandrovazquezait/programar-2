const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const mySchema = new Schema({
  tipo: {
    type: String,
    require: true,
  },
  info: String,
  alumno: String,
  actividad: String,
});

const model = mongoose.model("sor", mySchema);
module.exports = model;
