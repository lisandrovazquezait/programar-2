const store = require("./store");

function addMessage(curso, prof) {
  return new Promise((resolve, reject) => {
    const fullMessage = {
      curso: curso,
      prof: prof,
    };
    store.add(fullMessage);

    resolve(fullMessage);
  });
}

function getStop(filterStop) {
  return new Promise((resolve, reject) => {
    resolve(store.list(filterStop));
  });
}

function updateStop(id, info, parada) {
  return new Promise(async (resolve, reject) => {
    if (!id || !info) {
      reject("[controller.js updateMessage]Invalida data");
      resolve(id);
      return false;
    }
    const result = await store.update(id, info, parada);
    resolve(result);
  });
}
function deleteStop(paradas) {
  return new Promise((resolve, reject) => {
    if (!paradas) {
      reject("id invalido");
      return false;
    }
    store
      .delete(paradas)
      .then(() => {
        resolve();
      })
      .catch((e) => {
        reject(e);
      });
  });
}
module.exports = {
  addMessage,
  getStop,
  updateStop,
  deleteStop,
};
