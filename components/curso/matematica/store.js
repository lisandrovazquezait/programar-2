//const db = require("mongoose");
const Model = require("./model");

function addMatematica(parada) {
  // list.push(message);
  const miInfo = new Model(parada);
  miInfo.save();
}

async function getMatematica(tipo, actividad, alumno, info) {
  let filter = {};
  let parada = {};
  console.log(tipo);
  if (tipo !== null && tipo !== "prof") {
    filter = { tipo: tipo };
  } else if (actividad !== null) {
    filter = { actividad: actividad };
  } else if (alumno !== null) {
    filter = { alumno: alumno };
  } else if (info !== null) {
    filter = { info: info };
  }
  if (tipo === "prof") {
    parada = await Model.find();
    return parada;
  }
  parada = { data: await Model.find(filter) };
  return parada;
}

async function updateMatematica(id, actividad, info, rango) {
  if (rango !== undefined) {
    const collection = await Model.findById(id);
    let matematica = "matematica";
    let psocion = rango.indexOf(matematica);
    if (psocion !== -1) {
      if (actividad !== undefined) {
        collection.actividad = actividad;
      }
      if (info !== undefined) {
        collection.info = info;
      }
      const newMessage = await collection.save();

      return newMessage;
    } else {
      let johnny = `no esta inscripto`;
      return johnny;
    }
  } else {
    let johnny = `no esta inscripto`;
    return johnny;
  }
}

async function removeMatematica(id) {
  show = await Model.findById(id);
  console.log(show.tipo);
  return Model.findByIdAndDelete(id);
}

module.exports = {
  add: addMatematica,
  list: getMatematica,
  update: updateMatematica,
  delete: removeMatematica,
};
