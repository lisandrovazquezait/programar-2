const express = require("express");
const response = require("../../../network/response");
const controller = require("./controller");
const router = express.Router();

router.get("/", function (req, res) {
  const tipo = req.query.tipo || null;
  const actividad = req.query.actividad || null;
  const alumno = req.query.alumno || null;
  const info = req.query.info || null;
  const rango = req.body.rango;
  controller
    .getMatematica(tipo, actividad, alumno, info, rango)
    .then((johnny) => {
      response.success(req, res, johnny, 200);
    })
    .catch((e) => {
      response.error(req, res, "unexpected error", 500, e);
    });
});

router.post("/", function (req, res) {
  controller
    .addMatematica(
      req.body.tipo,
      req.body.alumno,
      req.body.info,
      req.body.actividad
    )
    .then((fullMessage) => {
      response.success(req, res, fullMessage, 201);
    })
    .catch((e) => {
      response.error(
        req,
        res,
        "Informacion invalida",
        400,
        "Error en el controfller"
      );
    });
});

router.patch("/:id", function (req, res) {
  controller
    .updateMatematica(
      req.params.id,
      req.body.actividad,
      req.body.info,
      req.body.rango
    )
    .then((data) => {
      response.success(req, res, data, 200);
    })
    .catch((e) => {
      response.error(req, res, "Error Interno network", 501, e);
    });
});

router.delete("/:_id", function (req, res) {
  const id = req.params;
  controller
    .deleteMatematica(id)

    .then((tipo) => {
      response.success(req, res, ` ${tipo.tipo} eliminado`, 200);
    })
    .catch((e) => {
      response.error(req, res, "Error interno", 500, e);
    });
});
module.exports = router;
