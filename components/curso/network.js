const express = require("express");
const response = require("../../network/response");
const controller = require("./controller");
const router = express.Router();

router.get("/", function (req, res) {
  const filterStop = req.query.curso || null;
  controller
    .getStop(filterStop)
    .then((parada) => {
      response.success(req, res, parada, 200);
    })
    .catch((e) => {
      response.error(req, res, "unexpected error", 500, e);
    });
});

router.post("/", function (req, res) {
  controller
    .addMessage(req.body.curso, req.body.prof)
    .then((fullMessage) => {
      response.success(req, res, fullMessage, 201);
    })
    .catch((e) => {
      response.error(
        req,
        res,
        "Informacion invalida",
        400,
        "Error en el controfller"
      );
    });
});

router.patch("/:id", function (req, res) {
  controller
    .updateStop(req.params.id, req.body.info, req.body.parada)
    .then((data) => {
      response.success(req, res, data, 200);
    })
    .catch((e) => {
      response.error(req, res, "Error Interno network", 501, e);
    });
});

router.delete("/", function (req, res) {
  console.log(req.query.parada);
  controller
    .deleteStop(req.query.parada)

    .then(() => {
      response.success(req, res, `Usuario ${req.query.parada} eliminado`, 200);
    })
    .catch((e) => {
      response.error(req, res, "Error interno", 500, e);
    });
});
module.exports = router;
