//const db = require("mongoose");
const Model = require("./model");

// db.Promise = global.Promise;
// db.connect("mongodb+srv://root:root@cluster0.d52lj.mongodb.net/test", {
//   useNewUrlParser: true,
// });
// console.log("[db] Conectada con exito");

function addCurso(curso) {
  // list.push(message);
  const nuevoCurso = new Model(curso);
  nuevoCurso.save();
}

async function getCurso(filterStop) {
  let filter = {};
  let parada = {};
  if (filterStop !== null) {
    filter = { parada: filterStop };
    parada = { data: await Model.find(filter) };
  } else {
    parada = await Model.find(filter);
  }
  return parada;
}

async function updateCurso(id, info, curso) {
  const foundMessage = await Model.findById(id);
  foundMessage.curso = curso;
  foundMessage.info = info;
  const newMessage = await foundMessage.save();

  return newMessage;
}

function removeCurso(para) {
  let filter = { parada: para };
  return Model.deleteOne(filter);
}

module.exports = {
  add: addCurso,
  list: getCurso,
  update: updateCurso,
  //delete: removeCurso,
};
