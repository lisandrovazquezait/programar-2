const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const mySchema = new Schema({
  curso: {
    type: String,
    require: true,
  },
  prof: String,
});

const model = mongoose.model("cursos", mySchema);
module.exports = model;
