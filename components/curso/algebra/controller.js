const store = require("./store");
const e = require("express");

function addAlgebra(tipo, alumno, info, actividad) {
  return new Promise((resolve, reject) => {
    const data = {
      tipo: tipo,
      alumno: alumno,
      info: info,
      actividad: actividad,
    };
    store.add(data);

    resolve(data);
  });
}

function getAlgebra(tipo, actividad, alumno, info, rango) {
  console.log(tipo, actividad, alumno, info);
  return new Promise((resolve, reject) => {
    if (null !== tipo || actividad || alumno || info) {
      resolve(store.list(tipo, actividad, alumno, info, rango));
    } else if (rango === "profesor") {
      let prof = "prof";
      resolve(store.list(prof));
    } else {
      reject(`no hay filtro`);
    }
  });
}

function updateAlgebra(id, actividad, info, rango) {
  return new Promise(async (resolve, reject) => {
    if (!id) {
      reject("[controller.js updateMessage]Invalida data");
      resolve(id);
      return false;
    }
    const result = await store.update(id, actividad, info, rango);
    resolve(result);
  });
}
function deleteAlgebra(id) {
  return new Promise((resolve, reject) => {
    if (!id) {
      reject("id invalido");
      return false;
    }
    store
      .delete(id)
      .then((tipo) => {
        resolve(tipo);
      })
      .catch((e) => {
        reject(e);
      });
  });
}
module.exports = {
  addAlgebra,
  getAlgebra,
  updateAlgebra,
  deleteAlgebra,
};
