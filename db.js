const db = require("mongoose");

db.Promise = global.Promise;
async function connect(url) {
  await db
    .connect(url, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
    })
    .catch((e) => {
      console.error(e);
    });

  console.log("[DB] Conectadad con Exito");
}
module.exports = connect;
