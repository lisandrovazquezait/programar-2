const express = require("express");
const bodyParser = require("body-parser");

const db = require("./db");
//const router = require("./components/message/network");
const router = require("./network/route");

db("mongodb+srv://root:root@cluster0.d52lj.mongodb.net/test");

var app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

router(app);

app.use("/app", express.static("public"));

app.listen(3005);
console.log("por http://localhost:3005");
